﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstTimeDelete.aspx.cs" Inherits="MstTimeDelete" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
 <script>
     $(document).ready(function() {
     $('#example').dataTable();
     $('#example2').dataTable();
     $('.datepicker').datepicker({
         format: "dd/mm/yyyy",
         autoclose: true
     });
     });
 </script>
 
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('#example2').dataTable();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Manual</a></li>
				<li class="active">Time Delete</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Time Delete </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Time Delete</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                        <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                           <div class="col-md-4">
                             <div class="form-group">
                              <label>Machine ID</label>
                              <asp:DropDownList runat="server" ID="txtMachineID" class="form-control select2" 
                                     style="width:100%;" AutoPostBack="true" onselectedindexchanged="txtMachineID_SelectedIndexChanged">
                              </asp:DropDownList>
                              
                            </div>
                           </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Employee Name</label>
								<asp:Label runat="server" ID="txtEmpName" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Department</label>
								<asp:Label runat="server" ID="txtDepartment" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 -->
                          </div>
                        <!-- end row -->
                        <legend>Time IN</legend>
                         <!-- begin row -->
                            <div class="row">
                              <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>TimeIN Date</label>
										<asp:TextBox runat="server" ID="txtTimeINDate" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
										 <asp:RequiredFieldValidator ControlToValidate="txtTimeINDate" Display="Dynamic"  ValidationGroup="ValidateIN_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                               </asp:RequiredFieldValidator>
									</div>
                                  </div>
                               <!-- end col-4 -->
                                               
                               <!-- begin col-4 -->
                               <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnTimeINView" Text="View" ValidationGroup="ValidateIN_Field"
                                         class="btn btn-success" onclick="btnTimeINView_Click" />
								 </div>
                                </div>
                               <!-- end col-4 -->
                                               
                            </div>
                         <!-- end row -->
                         <!-- table start -->
                         <div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>MachineID</th>
                                                <th>TimeIN</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("MachineID")%></td>
                                        <td><%# Eval("TimeIN")%></td>
                                        <td>
                                       <asp:LinkButton ID="btnTimeINEnquiry_Grid" class="btn btn-success btn-sm fa fa-check"  runat="server" 
                                           Text="" OnCommand="GridTimeINClick" CommandArgument='<%#Eval("TimeIN")%>' CommandName='<%# Eval("MachineID")%>'
                                           CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Delete this TimeIN details?');">
                                       </asp:LinkButton>
                                     </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					     </div>
                         <!-- table end -->
                          <legend>Time OUT</legend>
                         <!-- begin row -->
                            <div class="row">
                              <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>TimeOUT Date</label>
										<asp:TextBox runat="server" ID="txtTimeOUTDate" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
										<asp:RequiredFieldValidator ControlToValidate="txtTimeOUTDate" Display="Dynamic"  ValidationGroup="ValidateOUT_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                               </asp:RequiredFieldValidator>
									</div>
                                  </div>
                               <!-- end col-4 -->
                                               
                               <!-- begin col-4 -->
                               <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnTimeOUTView" Text="View" ValidationGroup="ValidateOUT_Field" 
                                         class="btn btn-success" onclick="btnTimeOUTView_Click" />
								 </div>
                                </div>
                               <!-- end col-4 -->
                                               
                            </div>
                         <!-- end row -->
                         <!-- table start -->
                         <div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example2" class="display table">
                                        <thead>
                                            <tr>
                                                <th>MachineID</th>
                                                <th>TimeOUT</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                       <td><%# Eval("MachineID")%></td>
                                       <td><%# Eval("TimeOUT")%></td>
                                       <td>
                                       <asp:LinkButton ID="btnTimeOUTEnquiry_Grid" class="btn btn-success btn-sm fa fa-check"  runat="server" 
                                           Text="" OnCommand="GridTimeOUTClick" CommandArgument='<%#Eval("TimeOUT")%>' CommandName='<%# Eval("MachineID")%>'
                                           CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Delete this TimeOUT details?');">
                                       </asp:LinkButton>
                                     </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					     </div>
                         <!-- table end -->
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>

