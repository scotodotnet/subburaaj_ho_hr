﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class MedicalDetails_Search : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    string SSQL;

    string mvarUserType;
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    String CurrentYear1;
    static int CurrentYear;
    string RptName;
    string Division;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Medical Details View";
                Load_Data_Medical_TransactionID();
                Load_Data_TokenID();
                btnSearch_Click(sender, e);
            }


            Load_OLD_data();
        }
    }

    private void Load_Data_TokenID()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        txtTokenNo.Items.Clear();
        query = "Select Distinct TokenID from MedicalDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " Order by TokenID Asc";

        DT = objdata.RptEmployeeMultipleDetails(query);
        txtTokenNo.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["TokenID"] = "-Select-";
        dr["TokenID"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtTokenNo.DataTextField = "TokenID";
        txtTokenNo.DataValueField = "TokenID";
        txtTokenNo.DataBind();
    }

    private void Load_Data_Medical_TransactionID()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        txtTransNo.Items.Clear();
        query = "Select Distinct cast(TransID as varchar(20)) as TransID from MedicalDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " Order by TransID Asc";

        DT = objdata.RptEmployeeMultipleDetails(query);
        txtTransNo.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["TransID"] = "-Select-";
        dr["TransID"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtTransNo.DataTextField = "TransID";
        txtTransNo.DataValueField = "TransID";
        txtTransNo.DataBind();
    }

    protected void btnDetails_Click(object sender, EventArgs e)
    {
        btnSearch_Click(sender, e);
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add(new DataColumn("TransID", typeof(string)));
        dt.Columns.Add(new DataColumn("TransDate", typeof(string)));
        dt.Columns.Add(new DataColumn("TokenID", typeof(string)));
        dt.Columns.Add(new DataColumn("Reason", typeof(string)));
        dt.Columns.Add(new DataColumn("DoctorName", typeof(string)));
        dt.Columns.Add(new DataColumn("Amount", typeof(string)));
        dt.Columns.Add(new DataColumn("Path_Name", typeof(string)));
        dt.Columns.Add(new DataColumn("File_Name", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Initial_Data_Referesh();

        //Search DirectPurchase
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select TransID,TransDate,TokenID,Reason,DoctorName,Amount,Path_Name,File_Name from MedicalDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        if (txtTransNo.SelectedValue != "-Select-")
        {
            query = query + " And TransID='" + txtTransNo.SelectedValue + "'";
        }
        if (txtTokenNo.SelectedValue != "-Select-")
        {
            query = query + " And TokenID='" + txtTokenNo.SelectedValue + "'";
        }
        if (txtFrmdate.Text != "" && txtTodate.Text != "")
        {
            query = query + " And Convert(Datetime, TransDate,103) >= CONVERT(Datetime,'" + txtFrmdate.Text + "',103)";
            query = query + " And Convert(Datetime, TransDate,103) <= CONVERT(Datetime,'" + txtTodate.Text + "',103)";
        }
        query = query + " Order by TransID,TokenID Asc";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {

            ViewState["ItemTable"] = Main_DT;
            Repeater1.DataSource = Main_DT;
            Repeater1.DataBind();


            //btnSave.Text = "Update";
        }
        else
        {
            //Clear_All_Field();
        }
    }

    protected void txtTransNo_SelectedIndexChanged(object sender, EventArgs e)
    {

        string query;
        DataTable DT = new DataTable();
        if (txtTransNo.SelectedItem.Text != "-Select-")
        {
            txtTokenNo.Items.Clear();
            query = "Select Distinct TokenID from MedicalDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And TransID='" + txtTransNo.SelectedValue + "'";
            query = query + " Order by TokenID Asc";

            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                txtTokenNo.DataSource = DT;
                DataRow dr = DT.NewRow();
                dr["TokenID"] = "-Select-";
                dr["TokenID"] = "-Select-";
                DT.Rows.InsertAt(dr, 0);
                txtTokenNo.DataTextField = "TokenID";
                txtTokenNo.DataValueField = "TokenID";
                txtTokenNo.DataBind();
            }
            else
            {
                Load_Data_TokenID();
            }
            
        }
        else
        {
            Load_Data_TokenID();
        }
    }

    protected void GridFileOpenClick(object sender, CommandEventArgs e)
    {
        string Filename = e.CommandName;
        string Folder_Name = e.CommandArgument.ToString();
        Folder_Name = Folder_Name.Replace('\\', '|').ToString();
        ResponseHelper.Redirect("newPage.aspx?fileName=" + Filename + "&FolderPath=" + Folder_Name, "_blank", "");
    }
}
