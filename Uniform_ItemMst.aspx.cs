﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Uniform_ItemMst : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Uniform Item Details";
            Load_ItemID();

        }
        Load_Data_Item();
    }

    private void Load_Data_Item()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Uniform_item_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Uniform_item_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemID='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtItemID.Text = DT.Rows[0]["ItemID"].ToString();
            txtItemName.Text = DT.Rows[0]["ItemName"].ToString();
            txtRate.Text = DT.Rows[0]["ItemRate"].ToString();            
            btnSave.Text = "Update";
        }
        else
        {
            txtItemName.Text = "";
            txtRate.Text = "0";
            Load_ItemID();
        }

    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Uniform_item_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemID='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            query = "Delete from Uniform_item_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ItemID='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Item Details Deleted Successfully');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Item Name Not Found');", true);
        }
        Load_Data_Item();
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();
        string Def_Chk = "";
        bool ErrFlag = false;

        if (txtItemName.Text.ToString() == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Item Name.!');", true);
        }
        if (txtRate.Text.ToString() == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Item Rate.!');", true);
        }
        
        if (!ErrFlag)
        {
            query = "select * from Uniform_item_mst where ItemID='" + txtItemID.Text + "' and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count > 0)
            {
                SaveMode = "Update";
                query = "delete from Uniform_item_mst where ItemID='" + txtItemID.Text + "' and Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }
            else
            {
                SaveMode = "Insert";
            }

            query = "Insert into Uniform_item_mst(Ccode,Lcode,ItemID,ItemName,ItemRate)";
            query = query + "values('" + SessionCcode + "','" + SessionLcode + "','" + txtItemID.Text + "','" + txtItemName.Text.ToUpper() + "','" + txtRate.Text + "')";
            objdata.RptEmployeeMultipleDetails(query);

            if (SaveMode == "Update")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Updated Successfully...!');", true);
            }
            else if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Saved Successfully...!');", true);
            }
            Clear_All_Field();
        }
        Load_Data_Item();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtItemName.Text = "";
        txtRate.Text = "0";
        btnSave.Text = "Save";
        Load_ItemID();
    }

    private void Load_ItemID()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Uniform_item_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by ItemID Desc";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            txtItemID.Text = (Convert.ToDecimal(DT.Rows[0]["ItemID"].ToString()) + Convert.ToDecimal(1)).ToString();
        }
        else
        {
            txtItemID.Text = "1";
        }
    }
}

