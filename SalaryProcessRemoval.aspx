﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master"  AutoEventWireup="true" CodeFile="SalaryProcessRemoval.aspx.cs" Inherits="SalaryProcessRemoval" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                         <ContentTemplate>--%>

<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
 <script>
     $(document).ready(function() {
     $('#example').dataTable();
     });
 </script>


<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                 $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };

</script>

<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Salary Process</a></li>
				<li class="active">Salary Process Removal</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Salary Process Removal</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Salary Process Removal</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                        <!-- begin row -->
                          <div class="row">
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								<label>Wages Type</label>
								 <asp:DropDownList runat="server" ID="ddlWagesType" class="form-control select2" style="width:100%;" 
								    AutoPostBack="true" OnSelectedIndexChanged="ddlWagesType_SelectedIndexChanged">
							 	 </asp:DropDownList>
							 	 </div>
								</div>
                               
                           <!-- end col-4 -->
                             <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Token No</label>
								 <asp:DropDownList runat="server" ID="ddlExistingCode" class="form-control select2" style="width:100%;"
								  > 
							 	 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								 <label>Month</label>
								 <asp:DropDownList runat="server" ID="ddlMonth" class="form-control select2" style="width:100%;"
								   AutoPostBack="true" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged">
								 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="January">January</asp:ListItem>
								 <asp:ListItem Value="February">February</asp:ListItem>
								 <asp:ListItem Value="March">March</asp:ListItem>
								 <asp:ListItem Value="April">April</asp:ListItem>
								 <asp:ListItem Value="May">May</asp:ListItem>
								 <asp:ListItem Value="June">June</asp:ListItem>
								 <asp:ListItem Value="July">July</asp:ListItem>
								 <asp:ListItem Value="August">August</asp:ListItem>
								 <asp:ListItem Value="September">September</asp:ListItem>
								 <asp:ListItem Value="October">October</asp:ListItem>
								 <asp:ListItem Value="November">November</asp:ListItem>
								 <asp:ListItem Value="December">December</asp:ListItem>
							 	 </asp:DropDownList>
								</div>
                             </div>
                         </div>
                        <!-- end row -->
                       <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                           <div class="col-md-4">
							   <div class="form-group">
							<label>Fin. Year</label>
								 <asp:DropDownList runat="server" ID="ddlFinYear" class="form-control select2" style="width:100%;"
								   AutoPostBack="true" OnSelectedIndexChanged="ddlFinYear_SelectedIndexChanged">
							 	 </asp:DropDownList>
								</div>
                             </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4">
							   <div class="form-group">
								<label>From Date</label>
										<asp:TextBox runat="server" ID="txtFromDate" class="form-control datepicker"></asp:TextBox>
								</div>
                             </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                         <div class="col-md-4">
							   <div class="form-group">
								<label>To Date</label>
										<asp:TextBox runat="server" ID="txtToDate" class="form-control datepicker"></asp:TextBox>
								</div>
                             </div>
                           <!-- end col-4 -->
                           </div>
                        <!-- end row -->
                        
                       
                          
                      <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<%----%>
									<asp:Button runat="server" id="btnDelete" Text="Delete" class="btn btn-success" 
                                         onclick="btnDelete_Click"  />
									
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                        
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
<%-- </ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="btnSave" />
</Triggers>
  </asp:UpdatePanel>--%>
 


</asp:Content>

