﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="BonusProcess.aspx.cs" Inherits="BonusProcess" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="javascript:;">Bonus</a></li>
            <li class="active">Bonus Process</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Bonus Process</h1>
        <!-- end page-header -->
        <asp:UpdatePanel ID="SalPay" runat="server">
            <ContentTemplate>
                <!-- begin row -->
                <div class="row">
                    <!-- begin col-12 -->
                    <div class="col-md-12">
                        <!-- begin panel -->
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                </div>
                                <h4 class="panel-title">Bonus Process</h4>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Category</label>
                                                <asp:DropDownList runat="server" ID="ddlCategory" class="form-control select2" AutoPostBack="true"
                                                    Style="width: 100%;" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged1">
                                                    <asp:ListItem Value="0">-Select-</asp:ListItem>
                                                    <asp:ListItem Value="1">Staff</asp:ListItem>
                                                    <asp:ListItem Value="2">Labour</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Employee Type</label>
                                                <asp:DropDownList runat="server" ID="ddlEmployeeType" class="form-control select2" Style="width: 100%;">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                          <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>From Month</label>
                                                <asp:DropDownList runat="server" ID="ddlFromMonth" class="form-control select2" Style="width: 100%;">
                                                     <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                    <asp:ListItem Value="January">January</asp:ListItem>
                                                    <asp:ListItem Value="February">February</asp:ListItem>
                                                    <asp:ListItem Value="March">March</asp:ListItem>
                                                    <asp:ListItem Value="April">April</asp:ListItem>
                                                    <asp:ListItem Value="May">May</asp:ListItem>
                                                    <asp:ListItem Value="June">June</asp:ListItem>
                                                    <asp:ListItem Value="July">July</asp:ListItem>
                                                    <asp:ListItem Value="August">August</asp:ListItem>
                                                    <asp:ListItem Value="September">September</asp:ListItem>
                                                    <asp:ListItem Value="October">October</asp:ListItem>
                                                    <asp:ListItem Value="November">November</asp:ListItem>
                                                    <asp:ListItem Value="December">December</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>From Year</label>
                                                <asp:DropDownList runat="server" ID="ddlFromyr" class="form-control select2" Style="width: 100%;">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                           <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>To Month</label>
                                                <asp:DropDownList runat="server" ID="ddlToMonth" class="form-control select2" 
                                                    Style="width: 100%;" >
                                                    <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                    <asp:ListItem Value="January">January</asp:ListItem>
                                                    <asp:ListItem Value="February">February</asp:ListItem>
                                                    <asp:ListItem Value="March">March</asp:ListItem>
                                                    <asp:ListItem Value="April">April</asp:ListItem>
                                                    <asp:ListItem Value="May">May</asp:ListItem>
                                                    <asp:ListItem Value="June">June</asp:ListItem>
                                                    <asp:ListItem Value="July">July</asp:ListItem>
                                                    <asp:ListItem Value="August">August</asp:ListItem>
                                                    <asp:ListItem Value="September">September</asp:ListItem>
                                                    <asp:ListItem Value="October">October</asp:ListItem>
                                                    <asp:ListItem Value="November">November</asp:ListItem>
                                                    <asp:ListItem Value="December">December</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>To Year</label>
                                                <asp:DropDownList runat="server" ID="ddlToYr" class="form-control select2" Style="width: 100%;">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->


                                    <!-- begin row -->
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnBonusProcess" Text="Bonus Process"
                                                    class="btn btn-success" OnClick="btnBonusProcess_Click" OnClientClick="ProgressBarShow();" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- end row -->

                                    <legend>Bonus Report</legend>

                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Category</label>
                                                <asp:DropDownList runat="server" ID="ddlcategory_Rpt" class="form-control select2" Style="width: 100%;" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlcategory_Rpt_SelectedIndexChanged">
                                                    <asp:ListItem Value="0">-Select-</asp:ListItem>
                                                    <asp:ListItem Value="1">Staff</asp:ListItem>
                                                    <asp:ListItem Value="2">Labour</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Employee Type</label>
                                                <asp:DropDownList runat="server" ID="ddlRptEmpType" class="form-control select2" Style="width: 100%;">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>From Month</label>
                                                <asp:DropDownList runat="server" ID="ddlRptFromMonth" class="form-control select2" 
                                                    Style="width: 100%;" >
                                                    <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                    <asp:ListItem Value="January">January</asp:ListItem>
                                                    <asp:ListItem Value="February">February</asp:ListItem>
                                                    <asp:ListItem Value="March">March</asp:ListItem>
                                                    <asp:ListItem Value="April">April</asp:ListItem>
                                                    <asp:ListItem Value="May">May</asp:ListItem>
                                                    <asp:ListItem Value="June">June</asp:ListItem>
                                                    <asp:ListItem Value="July">July</asp:ListItem>
                                                    <asp:ListItem Value="August">August</asp:ListItem>
                                                    <asp:ListItem Value="September">September</asp:ListItem>
                                                    <asp:ListItem Value="October">October</asp:ListItem>
                                                    <asp:ListItem Value="November">November</asp:ListItem>
                                                    <asp:ListItem Value="December">December</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>From Year</label>
                                                <asp:DropDownList runat="server" ID="txtRptFromYr" class="form-control select2" Style="width: 100%;">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>To Month</label>
                                                <asp:DropDownList runat="server" ID="ddlRptToMonth" class="form-control select2" 
                                                    Style="width: 100%;" >
                                                    <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                    <asp:ListItem Value="January">January</asp:ListItem>
                                                    <asp:ListItem Value="February">February</asp:ListItem>
                                                    <asp:ListItem Value="March">March</asp:ListItem>
                                                    <asp:ListItem Value="April">April</asp:ListItem>
                                                    <asp:ListItem Value="May">May</asp:ListItem>
                                                    <asp:ListItem Value="June">June</asp:ListItem>
                                                    <asp:ListItem Value="July">July</asp:ListItem>
                                                    <asp:ListItem Value="August">August</asp:ListItem>
                                                    <asp:ListItem Value="September">September</asp:ListItem>
                                                    <asp:ListItem Value="October">October</asp:ListItem>
                                                    <asp:ListItem Value="November">November</asp:ListItem>
                                                    <asp:ListItem Value="December">December</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>To Year</label>
                                                <asp:DropDownList runat="server" ID="txtRptToYr" class="form-control select2" Style="width: 100%;">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Salary Through</label>

                                                <asp:RadioButtonList ID="RdbCashBank" runat="server" RepeatColumns="3" class="form-control">
                                                    <asp:ListItem Selected="true" Text="ALL" style="padding-right: 40px" Value="0"></asp:ListItem>
                                                    <asp:ListItem Selected="False" Text="Cash" style="padding-right: 40px" Value="1"></asp:ListItem>
                                                    <asp:ListItem Selected="False" Text="Bank" Value="2"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Division</label>
                                                <asp:DropDownList runat="server" ID="ddlDivision" class="form-control select2" Style="width: 100%;">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>PF/Non PF</label>

                                                <asp:RadioButtonList ID="RdbPFNonPF" runat="server" RepeatColumns="3" class="form-control">
                                                    <asp:ListItem Selected="true" Text="ALL" style="padding-right: 40px" Value="0"></asp:ListItem>
                                                    <asp:ListItem Selected="False" Text="PF" style="padding-right: 40px" Value="1"></asp:ListItem>
                                                    <asp:ListItem Selected="False" Text="Non PF" Value="2"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Active Mode</label>

                                                <asp:RadioButtonList ID="RdbLeftType" runat="server" RepeatColumns="3" class="form-control">
                                                    <asp:ListItem Selected="true" Text="ALL" style="padding-right: 40px" Value="1"></asp:ListItem>
                                                    <asp:ListItem Selected="False" Text="OnRoll" style="padding-right: 40px" Value="2"></asp:ListItem>
                                                    <asp:ListItem Selected="False" Text="Left" Value="3"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-6 -->
                                        <div class="col-md-8" runat="server" visible="false">
                                            <div class="form-group">
                                                <label>Report Type</label>
                                                <asp:RadioButtonList ID="rdbPayslipFormat" runat="server" RepeatColumns="6">
                                                    <asp:ListItem Selected="true" Text="Individual" style="padding-right: 30px" Value="1"></asp:ListItem>
                                                    <asp:ListItem Selected="False" Text="Recociliation" style="padding-right: 30px" Value="2"></asp:ListItem>
                                                    <asp:ListItem Selected="False" Text="Checklist" style="padding-right: 30px" Value="3"></asp:ListItem>
                                                    <asp:ListItem Selected="False" Text="Signlist" style="padding-right: 30px" Value="4"></asp:ListItem>
                                                    <asp:ListItem Selected="False" Text="Blank Signlist" style="padding-right: 30px" Value="5"></asp:ListItem>
                                                    <asp:ListItem Selected="False" Text="Cover" Value="6"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <!-- end col-6 -->
                                        <tr id="GridView" runat="server" visible="false">
                                            <td colspan="5">
                                                <asp:GridView ID="GridExcelView" runat="server" AutoGenerateColumns="true">
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </div>
                                    <!-- end row -->

                                    <!-- begin row -->
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnBonusView" Text="Report View"
                                                    class="btn btn-success" OnClick="btnBonusView_Click" Visible="false" />
                                                <asp:Button runat="server" ID="btnBonusExcelView" Text="Excel View"
                                                    class="btn btn-success" OnClick="btnBonusExcelView_Click" Visible="false" />
                                                <asp:Button runat="server" ID="btnReport" Text="Excel"
                                                    class="btn btn-success" OnClick="btnReport_Click" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4"></div>
                                    </div>
                                    <div id="Download_loader" style="display: none" />
                                </div>
                                <!-- end row -->

                                <asp:Panel runat="server" ID="Panel2" Visible="false">
                                    <tr id="Tr2" runat="server" visible="false">
                                        <td colspan="4">
                                            <asp:GridView ID="BankGV" runat="server" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>S. No</HeaderTemplate>
                                                        <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Token No</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTOkenNo" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField>
                                                        <HeaderTemplate>NAME</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>DOJ</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDoj" runat="server" Text='<%# Eval("DOJ") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Days</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDays" runat="server" Text='<%# Eval("Worked_Days") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                      <asp:TemplateField>
                                                        <HeaderTemplate>Gross Salary</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGross" runat="server" Text='<%# Eval("Gross_Sal") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Bonus %</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblBonusPercent" runat="server" Text='<%# Eval("Bonus_Percent") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>Bonus Amt</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblBonusAmt" runat="server" Text='<%# Eval("Bonus_Amt") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                      <asp:TemplateField>
                                                        <HeaderTemplate>Final Bonus Amt</HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFinalBonus" runat="server" Text='<%# Eval("Final_Bonus_Amt") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnBonusExcelView" />
                <asp:PostBackTrigger ControlID="btnReport" />
            </Triggers>
        </asp:UpdatePanel>

        <!-- end row -->
    </div>
    <!-- end #content -->
     <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                }
            });
        };
    </script>
    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            swal(msg);
        }
    </script>

    <script type="text/javascript">
        function ProgressBarShow() {
            $('#Download_loader').show();
        }
    </script>

    <script type="text/javascript">
        function ProgressBarHide() {
            $('#Download_loader').hide();
        }
    </script>
    <!-- begin #content -->
</asp:Content>

