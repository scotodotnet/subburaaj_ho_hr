﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="Training_WorkDays_Report.aspx.cs" Inherits="Training_WorkDays_Report" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
     function SaveMsgAlert(msg) {
         swal(msg);
     }
     $(document).ready(function () {
         App.init();
         FormWizard.init();
         //FormWizardValidation.init();
         $('.select2').select2();
         $('.datepicker').datepicker({
             format: "dd/mm/yyyy",
             autoclose: true
         });

     });
</script>



<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Report</a></li>
				<li class="active">Trainee Work Days Report</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Trainee Work Days Report</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                  <ContentTemplate>
                  <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Trainee Work Days Report</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                       
                       <!-- begin row -->
                        <div class="row">
                          <!-- begin col-4 -->
                              <div id="Div1" class="col-md-4" runat="server">
								<div class="form-group">
								 <label>Category</label>
								 <asp:DropDownList runat="server" ID="ddlCategory" class="form-control select2" style="width:100%;"
								    onselectedindexchanged="ddlCategory_SelectedIndexChanged" AutoPostBack="true">
								 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="Staff">Staff</asp:ListItem>
								 <asp:ListItem Value="Labour">Labour</asp:ListItem>
								</asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div id="Div2" class="col-md-4" runat="server">
								<div class="form-group">
								 <label>Employee Type</label>
								 <asp:DropDownList runat="server" ID="ddlEmployeeType" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Date</label>
										<asp:TextBox runat="server" ID="txtWorkDays_Date" class="form-control datepicker"></asp:TextBox>
									</div>
                                  </div>
                               <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                      
                      
                       <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />									
									<asp:Button runat="server" id="btnReport" Text="Report" OnClick="btnReport_Click" class="btn btn-success" />
						    	 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                        </div>
                        </div>
                         
                    </div>
                    <!-- end panel -->
                </div>
                  </ContentTemplate>
                </asp:UpdatePanel>
			    
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->

</asp:Content>

