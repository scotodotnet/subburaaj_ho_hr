﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="ScmReelingWorkloadEntry.aspx.cs" Inherits="ScmReelingWorkloadEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="javascript:;">Reeling Process</a></li>
            <li class="active">Reeling WorkLaod Entry</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Reeling WorkLoad Entry</h1>
        <!-- end page-header -->

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="col-md-12">
                        <!-- begin panel -->
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                </div>
                                <h4 class="panel-title">Reeling WorkLoad Entry</h4>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Fin. Year</label>
                                                <asp:DropDownList runat="server" ID="ddlfinance" class="form-control select2" AutoPostBack="true" OnSelectedIndexChanged="ddlfinance_SelectedIndexChanged"
                                                    Style="width: 100%;">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlfinance" TabIndex="1" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Token No.</label>
                                                <asp:DropDownList runat="server" ID="ddlExistingCode" class="form-control select2" AutoPostBack="true" OnSelectedIndexChanged="ddlExistingCode_SelectedIndexChanged"
                                                    Style="width: 100%;">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlExistingCode" TabIndex="2" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                          <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <asp:TextBox runat="server" ID="txtEmpName" class="form-control" AutoComplete="off" Enabled="false" Font-Bold="true"></asp:TextBox>
                                              
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Date</label>
                                                <asp:TextBox runat="server" ID="txtDate" class="form-control datepicker" TabIndex="3" AutoComplete="off" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Target Workload</label>
                                                <asp:DropDownList runat="server" ID="ddlTarget" class="form-control select2" TabIndex="4" AutoPostBack="true"
                                                    Style="width: 100%;">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlTarget" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Achived Workload</label>
                                                <asp:TextBox runat="server" ID="txtWorkloadAchive" class="form-control" AutoComplete="off" TabIndex="5" Text="0"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtWorkloadAchive" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->

                                    <!-- begin row -->
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnSave" Text="Save" OnClick="btnSave_Click" ValidationGroup="Validate_Field" TabIndex="6" class="btn btn-success" />
                                                <asp:Button ID="btnclear" runat="server" Text="Clear" class="btn btn-danger" TabIndex="7"
                                                    OnClick="btnclear_Click" />
                                                <asp:Button runat="server" ID="btnDelete" Text="Delete" OnClick="btnDelete_Click" TabIndex="8" class="btn btn-success" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- end row -->
                                </div>
                            </div>
                            <div id="Download_loader" style="display: none" />
                        </div>
                    </div>
                    <!-- end panel -->
                    </div>
                 
                </ContentTemplate>
            </asp:UpdatePanel>

            <!-- end col-12 -->
        </div>
        <!-- end row -->
    </div>
    <!-- end #content -->



    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                }
            });
        };
    </script>
    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            swal(msg);
        }
    </script>

    <script type="text/javascript">
        function ProgressBarShow() {
            $('#Download_loader').show();
        }
    </script>
    <style>
        .textmode {
            mso-number-format: \@;
        }
    </style>

    <script type="text/javascript">
        function ProgressBarHide() {
            $('#Download_loader').hide();
        }
    </script>

</asp:Content>

