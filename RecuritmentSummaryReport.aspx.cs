﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;

public partial class RecuritmentSummaryReport : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;

    string SSQL;
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();
    DataTable dt1 = new DataTable();

    BALDataAccess objdata = new BALDataAccess();
    System.Web.UI.WebControls.DataGrid grid =
                          new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Recuirtment Summary Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Division = Request.QueryString["Division"].ToString();




            DataCell.Columns.Add("SI.No");
            DataCell.Columns.Add("RecuirtmentThrough");
            DataCell.Columns.Add("RecuirtmentName");
            DataCell.Columns.Add("RecuirtmentUnit");
            DataCell.Columns.Add("MobileNumber");
            DataCell.Columns.Add("Total Strength");



            SSQL = "Select RecuritmentThro,RecuriterName,RecuritmentUnit,RecutersMob,Count(EmpNo) as[Total_Employee]";
            SSQL = SSQL + "from Employee_Mst  where LocCode='"+SessionLcode +"' and CompCode='"+SessionCcode +"' ";
            SSQL = SSQL + " and IsActive='Yes' group by RecuritmentThro,RecuriterName,RecuritmentUnit,RecutersMob";
            if (Division != "-Select-")
            {
                SSQL = SSQL + " and EM.Division='" + Division + "' ";
            }
             dt = objdata.RptEmployeeMultipleDetails(SSQL);
             if (dt.Rows.Count != 0)
             {
                 

                 for (int m = 0; m < dt.Rows.Count; m++)
                 {
                     DataCell.NewRow();
                     DataCell.Rows.Add();
                     DataCell.Rows[m]["SI.No"] = m + 1;
                     DataCell.Rows[m]["RecuirtmentThrough"] = dt.Rows[m]["RecuritmentThro"].ToString();
                     DataCell.Rows[m]["RecuirtmentName"] = dt.Rows[m]["RecuriterName"].ToString();
                     DataCell.Rows[m]["RecuirtmentUnit"] = dt.Rows[m]["RecuritmentUnit"].ToString();
                     DataCell.Rows[m]["MobileNumber"] = dt.Rows[m]["RecutersMob"].ToString();
                     DataCell.Rows[m]["Total Strength"] = dt.Rows[m]["Total_Employee"].ToString();
                 }

                  SSQL = "Select * from Company_Mst ";
            dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
            string name = dt1.Rows[0]["CompName"].ToString();

            grid.DataSource = DataCell;
            grid.DataBind();
            string attachment = "attachment;filename=Recuirtment Summary Report.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table border='1'>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='6'>");
            Response.Write("<a style=\"font-weight:bold\">" + name + " - " + SessionLcode + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='6'>");
            Response.Write("<a style=\"font-weight:bold\">Recuritment Summary Report</a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            //Response.Write("<tr Font-Bold='true' align='center'>");
            //Response.Write("<td colspan='6'>");
            //Response.Write("<a style=\"font-weight:bold\"></a>");

            //Response.Write("</td>");
            //Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            for (int k = 0; k < DataCell.Columns.Count; k++)
            {

                Response.Write("<td colspan='1'>");
                Response.Write("<a style=\"font-weight:bold\">" + DataCell.Columns[k] + " </a>");

                Response.Write("</td>");

            }
            Response.Write("</tr>");

            for (int n = 0; n < DataCell.Rows.Count; n++)
            {
              
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["SI.No"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["RecuirtmentThrough"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["RecuirtmentName"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["RecuirtmentUnit"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["MobileNumber"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Total Strength"].ToString() + " </a>");
                Response.Write("</td>");
               

                Response.Write("</tr>");
            }
            Response.Write("</table>");
            //Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Found..');", true);
        }


             }
        }
    }
