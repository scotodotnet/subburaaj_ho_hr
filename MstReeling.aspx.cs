﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;


public partial class MstReeling : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SessionEpay;
    bool ErrFlg = false;
    SqlConnection con;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];

    string SSQL = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "Payroll::Reeling Master";
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
                Get_Last_Code();
            }
        }
        Load_Data();
    }

    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstReeling where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }

    private void Get_Last_Code()
    {
        con = new SqlConnection(constr);
        string getNo = "0";
        SSQL = "";
        SSQL = "select isnull((Select Max(Code) from [" + SessionEpay + "]..MstReeling where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'),'0') as Code";
        SqlCommand Cmd = new SqlCommand(SSQL, con);
        con.Open();
        getNo = Cmd.ExecuteScalar().ToString();
        con.Close();
        if (getNo == "0" || getNo == "" || getNo == string.Empty || getNo == null)
        {
            getNo = "1";
        }
        else
        {
            getNo = (Convert.ToInt32(getNo) + Convert.ToInt32(1)).ToString();
        }

        txtCode.Text = getNo.ToString();
    }

    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string _code = e.CommandArgument.ToString();
        string _finYearVal = e.CommandName.ToString();

        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstReeling where Ccode='" + SessionCcode + "'and Lcode='" + SessionLcode + "' and Code='" + _code + "' and FinYearVal='" + _finYearVal + "'";
        DataTable dt_getData = new DataTable();
        dt_getData = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_getData.Rows.Count > 0)
        {
            ddlfinance.SelectedValue = dt_getData.Rows[0]["FinYearCode"].ToString();
            txtCode.Text = dt_getData.Rows[0]["Code"].ToString();
            txtWorkLoad.Text= dt_getData.Rows[0]["Target"].ToString();
            btnSave.Text = "Update";
        }
        Load_Data();
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string _code = e.CommandArgument.ToString();
        string _finYearVal = e.CommandName.ToString();

        SSQL = "";
        SSQL = "Delete from [" + SessionEpay + "]..MstReeling where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Code='" + _code + "' and FinYearVal='" + _finYearVal + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "swal", "SaveMsgAlert('The Workload Item Deleted Successfully')", true);
        Load_Data();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ddlfinance.SelectedItem.Text == "-Select-")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "swal", "SaveMsgAlert('Please Choose the Financial Year')", true);
            return;
        }
        if (txtCode.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "swal", "SaveMsgAlet('Please Entre the Code')", true);
            return;
        }
        if (txtWorkLoad.Text == "")
        {
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "swal", "SaveMsgAlert('Please Enter the Workload')", true);
            return;
        }
        if (!ErrFlg)
        {
            SSQL = "";
            SSQL = "Select * from [" + SessionEpay + "]..MstReeling where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and FinYearCode='" + ddlfinance.SelectedValue + "' and Target='" + txtWorkLoad.Text + "'";
            DataTable dt_check = new DataTable();
            dt_check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_check.Rows.Count > 0)
            {
                if (btnSave.Text == "Update")
                {
                    SSQL = "";
                    SSQL = "Delete from [" + SessionEpay + "]..MstReeling where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and FinYearCode='" + ddlfinance.SelectedValue + "' and Target='" + txtWorkLoad.Text + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
                else
                {
                    ErrFlg = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "swal", "SaveMsgAlert('The Target Workload is Already Present ')", true);
                    return;
                }
            }
            if (!ErrFlg)
            {
                GetIPAndName getIPAndName = new GetIPAndName();
                string _IP = getIPAndName.GetIP();
                string _HostName = getIPAndName.GetName();

                SSQL = "";
                SSQL = "Insert into ["+SessionEpay+ "]..MstReeling (Ccode,Lcode,FinYearCode,FinYearVal,Code,Target,CreatedBy,CreatedOn,HostIP,HostName)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + ddlfinance.SelectedValue + "','" + ddlfinance.SelectedItem.Text + "','" + txtCode.Text + "',";
                SSQL = SSQL + "'" + txtWorkLoad.Text + "','" + SessionUserID + "','" + DateTime.Now.ToString("dd/MM/yyyy hh:mm tt") + "','" + _IP + "','" + _HostName + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "swal", "SaveMsgAlert('The Target Workload is Saved Successfully ')", true);
                btnClr_Click(sender, e);
            }
        }
    }

    protected void btnClr_Click(object sender, EventArgs e)
    {
        ddlfinance.ClearSelection();
        txtCode.Text = "";
        Get_Last_Code();
        txtWorkLoad.Text = "0";
        btnSave.Text = "Save";
        Load_Data();
    }
}